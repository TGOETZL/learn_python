class Car:

    # private
    __type = ''
    __color = ''

    # public
    doors = 0

    # constructor
    def __init__(self, t, f):
        self.__type = t
        self.__color = f

    def get_color(self):
        return self.__color

    def get_type(self):
        return  self.__type

    def set_doors(self, d):
        self.doors = d
