from Car import Car


if __name__ == "__main__":

    obj1 = Car("Porsche", "black")
    print("Type: " + obj1.get_type() + "  Color: " + obj1.get_color())

    obj1.set_doors(2)
    print(obj1.doors)
