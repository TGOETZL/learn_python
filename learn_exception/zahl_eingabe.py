def division():

    while True:

        try:
            z1 = float(input("Bitte geben Sie die erste Zahl ein:\n"))
            z2 = float(input("Bitte geben Sie die zweite Zahl ein\n"))

            erg = z1 / z2

            print(erg)

        except ZeroDivisionError:
            print("Div durch 0")

        except ValueError:
            print("Bitte geben Sie einen float oder int ein")

        except BaseException:
            "Error"

        if(input("\nAbbruch mit x:\n") == 'x'):
            break


if __name__ == '__main__':
    division()